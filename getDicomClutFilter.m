//
//  getDicomClutFilter.h
//  getDicomClut
//
//  Copyright (c) 2014 Konrad Werys. All rights reserved.
//

#import "getDicomClutFilter.h"

// @TODO: test if it works correctly with 8bit map

@implementation getDicomClutFilter

- (void) initPlugin{
}

- (void) willUnload{
    NSLog(@"getDicomClutFilter willUnload");
}

/**
 Does it ever go here? I cant see it logging. Where should I release the memebers?
 */
- (void)dealloc{
    NSLog(@"getDicomClutFilter dealloc");
//    [_arrayRed8 release];
//    [_arrayGreen8 release];
//    [_arrayBlue8 release];
//    [_arrayRed16 release];
//    [_arrayGreen16 release];
//    [_arrayBlue16 release];
    
    [super dealloc];
}

/**
 fills lut arrays (members called array<Red/Green/Blue><8/16>)
 */
- (int) getDicomClut{
    ViewerController *vc = [ViewerController frontMostDisplayed2DViewer];
    NSArray         *pixList = [vc pixList: 0];
    DCMPix          *curPix = [pixList objectAtIndex: 0];
    
    DCMObject *dcmObj = [DCMObject objectWithContentsOfFile:[curPix sourceFile] decodingPixelData:NO];

    // decoding descriptors as explained in http://dicom.nema.org/medical/dicom/2014c/output/chtml/part03/sect_C.7.6.3.html#sect_C.7.6.3.1.5
    NSArray *dcmTagContentRedDescArray = [dcmObj attributeArrayWithName:@"RedPaletteColorLookupTableDescriptor"];
    NSArray *dcmTagContentGreenDescArray = [dcmObj attributeArrayWithName:@"GreenPaletteColorLookupTableDescriptor"];
    NSArray *dcmTagContentBlueDescArray = [dcmObj attributeArrayWithName:@"BluePaletteColorLookupTableDescriptor"];
    
    // exit in case the tag is empty
    if (!dcmTagContentRedDescArray
        || !dcmTagContentGreenDescArray
        || !dcmTagContentBlueDescArray){
        return EXIT_FAILURE;
    }
    
    if ([dcmTagContentBlueDescArray count] == 0
        || [dcmTagContentGreenDescArray count] == 0
        || [dcmTagContentBlueDescArray count] == 0){
        return EXIT_FAILURE;
    }
    
    // supporting variables numberOfEntriesInLut
    NSNumber *numberOfEntriesInLutRed   = [dcmTagContentRedDescArray objectAtIndex:0];
    NSNumber *numberOfEntriesInLutGreen = [dcmTagContentGreenDescArray objectAtIndex:0];
    NSNumber *numberOfEntriesInLutBlue  = [dcmTagContentBlueDescArray objectAtIndex:0];
    
    // supporting variables firstInputValueMapped
    NSNumber *firstInputValueMappedRed   = [dcmTagContentRedDescArray objectAtIndex:1];
    NSNumber *firstInputValueMappedGreen = [dcmTagContentGreenDescArray objectAtIndex:1];
    NSNumber *firstInputValueMappedBlue  = [dcmTagContentBlueDescArray objectAtIndex:1];
    
    // supporting variables numberOfBitsPerEntry
    NSNumber *numberOfBitsPerEntryRed   = [dcmTagContentRedDescArray objectAtIndex:2];
    NSNumber *numberOfBitsPerEntryGreen = [dcmTagContentGreenDescArray objectAtIndex:2];
    NSNumber *numberOfBitsPerEntryBlue  = [dcmTagContentBlueDescArray objectAtIndex:2];
    
    // getting the color tables as data
    NSMutableData *dcmTagContentRedLutData   = [[dcmObj attributeWithName:@"RedPaletteColorLookupTableData"] value];
    NSMutableData *dcmTagContentGreenLutData = [[dcmObj attributeWithName:@"GreenPaletteColorLookupTableData"] value];
    NSMutableData *dcmTagContentBlueLutData  = [[dcmObj attributeWithName:@"BluePaletteColorLookupTableData"] value];
    
    // exit in case the tag is empty
    if ([dcmTagContentRedLutData length] == 0
        || [dcmTagContentGreenLutData length] == 0
        || [dcmTagContentBlueLutData length] == 0){
        return EXIT_FAILURE;
    }
    
    // getting the color tables as array
    NSMutableArray *arrayRed   = [NSMutableArray arrayWithCapacity: [numberOfEntriesInLutRed intValue]];
    NSMutableArray *arrayGreen = [NSMutableArray arrayWithCapacity: [numberOfEntriesInLutGreen intValue]];
    NSMutableArray *arrayBlue  = [NSMutableArray arrayWithCapacity: [numberOfEntriesInLutBlue intValue]];
    
    // RED
    for (int i = 0; i < [numberOfEntriesInLutRed intValue]; i++){
        uint16 buffer;
        int position = i * [numberOfBitsPerEntryRed intValue]/8;
        [dcmTagContentRedLutData getBytes: &buffer range: NSMakeRange(position, [numberOfBitsPerEntryRed intValue]/8)];
        [arrayRed addObject: [NSNumber numberWithInt: buffer]];
    }
    
    // GREEN
    for (int i = 0; i < [numberOfEntriesInLutGreen intValue]; i++){
        uint16 buffer;
        int position = i * [numberOfBitsPerEntryGreen intValue]/8;
        [dcmTagContentGreenLutData getBytes: &buffer range: NSMakeRange(position, [numberOfBitsPerEntryGreen intValue]/8)];
        [arrayGreen addObject: [NSNumber numberWithInt: buffer]];
    }
    
    // BLUE
    for (int i = 0; i < [numberOfEntriesInLutBlue intValue]; i++){
        uint16 buffer;
        int position = i * [numberOfBitsPerEntryBlue intValue]/8;
        [dcmTagContentBlueLutData getBytes: &buffer range: NSMakeRange(position, [numberOfBitsPerEntryBlue intValue]/8)];
        [arrayBlue addObject: [NSNumber numberWithInt: buffer]];
    }
    
    // fill the members
    _arrayRed8   = [NSMutableArray arrayWithArray: arrayRed];
    _arrayGreen8 = [NSMutableArray arrayWithArray: arrayGreen];
    _arrayBlue8  = [NSMutableArray arrayWithArray: arrayBlue];
    
    // fill the members
    _arrayRed16   = [NSMutableArray arrayWithArray: arrayRed];
    _arrayGreen16 = [NSMutableArray arrayWithArray: arrayGreen];
    _arrayBlue16  = [NSMutableArray arrayWithArray: arrayBlue];
    
    // rescale
    if ([numberOfBitsPerEntryRed intValue] == 16){
        for (int i = 0; i < _arrayRed8.count; ++i){
            [_arrayRed8 replaceObjectAtIndex:i withObject: [NSNumber numberWithInt: [[arrayRed objectAtIndex:i] intValue] / 256]];
        }
    }
    if ([numberOfBitsPerEntryGreen intValue] == 16){
        for (int i = 0; i < _arrayGreen8.count; ++i){
            [_arrayGreen8 replaceObjectAtIndex:i withObject: [NSNumber numberWithInt: [[arrayGreen objectAtIndex:i] intValue] / 256]];
        }
    }
    if ([numberOfBitsPerEntryBlue intValue] == 16){
        for (int i = 0; i < _arrayBlue8.count; ++i){
            [_arrayBlue8 replaceObjectAtIndex:i withObject: [NSNumber numberWithInt: [[arrayBlue objectAtIndex:i] intValue] / 256]];
        }
    }

    if ([numberOfBitsPerEntryGreen intValue] == 8){
        for (int i = 0; i < _arrayRed16.count; ++i){
            [_arrayRed16 replaceObjectAtIndex:i withObject: [NSNumber numberWithInt: [[arrayRed objectAtIndex:i] intValue] * 256]];
        }
    }
    if ([numberOfBitsPerEntryGreen intValue] == 8){
        for (int i = 0; i < _arrayGreen16.count; ++i){
            [_arrayGreen16 replaceObjectAtIndex:i withObject: [NSNumber numberWithInt: [[arrayGreen objectAtIndex:i] intValue] * 256]];
        }
    }
    if ([numberOfBitsPerEntryBlue intValue] == 8){
        for (int i = 0; i < _arrayBlue16.count; ++i){
            [_arrayBlue16 replaceObjectAtIndex:i withObject: [NSNumber numberWithInt: [[arrayBlue objectAtIndex:i] intValue] * 256]];
        }
    }
    
    // printing
//    NSMutableString *mysting = [NSMutableString string];
//    for (int i = 0; i < [_arrayRed8 count] ; ++i){
//        [mysting appendString: [NSString stringWithFormat:@"%@, ",_arrayRed8[i]]];
//    }
//    NSLog(@"%@", mysting);
    
    return EXIT_SUCCESS;
}

/**
 apply clut to viewerController
 */
- (int) applyClut{
    
    // exit if empty
    if (!_arrayRed8
        || !_arrayGreen8
        || !_arrayBlue8){
        return EXIT_FAILURE;
    }
    
    if ([_arrayRed8 count] == 0
        || [_arrayGreen8 count] == 0
        || [_arrayBlue8 count] == 0){
        return EXIT_FAILURE;
    }
    
    // basicly I copied the code from ViewerController.m -endCLUT
    NSMutableDictionary *clutDict        = [[[[NSUserDefaults standardUserDefaults] dictionaryForKey: @"CLUT"] mutableCopy] autorelease];
    NSMutableDictionary *aCLUTFilter    = [NSMutableDictionary dictionary];
    NSMutableArray        *rArray = [NSMutableArray array];
    NSMutableArray        *gArray = [NSMutableArray array];
    NSMutableArray        *bArray = [NSMutableArray array];
    
    for( int i = 0; i < 256; ++i) [rArray addObject: [_arrayRed8   objectAtIndex: i * 16] ];
    for( int i = 0; i < 256; ++i) [gArray addObject: [_arrayGreen8 objectAtIndex: i * 16] ];
    for( int i = 0; i < 256; ++i) [bArray addObject: [_arrayBlue8  objectAtIndex: i * 16] ];
    
    [aCLUTFilter setObject:rArray forKey:@"Red"];
    [aCLUTFilter setObject:gArray forKey:@"Green"];
    [aCLUTFilter setObject:bArray forKey:@"Blue"];
    
    [clutDict setObject: aCLUTFilter forKey: @"last getDicomClut plugin run"];
    [[NSUserDefaults standardUserDefaults] setObject: clutDict forKey: @"CLUT"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: OsirixUpdateCLUTMenuNotification
                                                        object: [[ViewerController frontMostDisplayed2DViewer] curCLUTMenu]
                                                      userInfo: [NSDictionary dictionary]];
    
    [[ViewerController frontMostDisplayed2DViewer] ApplyCLUTString:@"last getDicomClut plugin run"];
    
    return EXIT_SUCCESS;
}

/**
 what happens after pressing the clut menu item
 */
- (void) menuItemAction{
    
    [self getDicomClut];
    [self applyClut];
}

/**
 filter function - adds the new clut menu item
 */
- (long) filterImage:(NSString*) menuName
{
    _arrayRed8 = nil;
    _arrayGreen8 = nil;
    _arrayBlue8 = nil;
    _arrayRed16 = nil;
    _arrayGreen16 = nil;
    _arrayBlue16 = nil;
    
    [self menuItemAction];
 
////  adding a button
//    NSLog(@"Here");
//    NSPopUpButton *clutPopup = [[ViewerController frontMostDisplayed2DViewer] valueForKey:@"clutPopup"];
//    NSMenuItem *myMenuItem = [[NSMenuItem alloc] init];
//    [myMenuItem setTitle:NSLocalizedString(@"CLUT from DICOM", nil)];
//    [myMenuItem setTarget:self];
//    [myMenuItem setAction:@selector(menuItemAction)];
//    //NSLog(@"emptyFun %@",[[myMenuItem target] respondsToSelector:@selector(emptyFun)] ? @"true" : @"false");
//    [[clutPopup menu] addItem:myMenuItem];
//    NSLog(@"Here");
    
////  nice message
//    NSAlert *myAlert = [NSAlert alertWithMessageText:@"getDicomClut"
//                                       defaultButton:@"OK"
//                                     alternateButton:nil
//                                         otherButton:nil
//                           informativeTextWithFormat:@"DicomClut added, check your CLUT list."];
//    [myAlert runModal];
    
    return 0;
}

@end
