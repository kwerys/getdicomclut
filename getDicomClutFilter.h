//
//  getDicomClutFilter.h
//  getDicomClut
//
//  Copyright (c) 2014 Konrad Werys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OsiriXAPI/PluginFilter.h>
#import <OsiriXAPI/Notifications.h>

#import "DCMObject.h"
#import "DCMAttribute.h"
#import "DCMAttributeTag.h"

@interface getDicomClutFilter : PluginFilter {
}

@property (nonatomic,assign) NSMutableArray* arrayRed8;
@property (nonatomic,assign) NSMutableArray* arrayGreen8;
@property (nonatomic,assign) NSMutableArray* arrayBlue8;

@property (nonatomic,assign) NSMutableArray* arrayRed16;
@property (nonatomic,assign) NSMutableArray* arrayGreen16;
@property (nonatomic,assign) NSMutableArray* arrayBlue16;

- (long) filterImage:(NSString*) menuName;

@end
